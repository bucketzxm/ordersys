/**
 * Created by simon on 7/3/15.
 */

$(function(){

    $("#sumBar span:eq(0)").click(//点击现金支付
        function () {

             $.ajax({
                type: 'GET',
                url: 'makeOrder/',
                success: function(result){
                    $("#cash.popQuery strong").text(result);
                    $(".mask").show();
			        $("#cash").show();
                },
                error: function(e){
                    console.log("Get Order Number Error " + e);
                    location.href('/myDish');
                }
            });
        }
    );
    $("#sumBar span:eq(1)").click(//点击在线支付
        function () {
            $.ajax({
                type: 'GET',
                url: 'makeOrder/',
                success: function(result){
                    console.log(result);
                    $("#online.popQuery strong").text(result);
                    $(".mask").show();
			        $("#online").show();
                },
                error: function(e){
                    console.log("Get Order Number Error " + e);
                    location.href = '/myDish';
                }

            });

        }
    );


    $("#sumBar span:eq(2)").click(  // 点击稍后支付
        function () {
            location.href = "/"
        }
    );


    $("#cash.popQuery th:eq(0)").click( //点击弹出框下载app － 是
        function () {
            location.href = '/media/app/WeiyueOrderSys.app';
        }
    );

    $("#cash.popQuery th:eq(1)").click( //点击弹出框下载app - 否
        function(){
            location.href = '/confirmOrder';
        }
    );
    $("#online.popQuery th:eq(0)").click( //点击弹出框在线支付 - 是
        function(){
            location.href = '/choosePayMethods';
        }
    );

    $("#online.popQuery th:eq(1)").click( //点击弹出框在线支付 - 否
        function(){
            location.href = "/confirmOrder";
        }
    );
});