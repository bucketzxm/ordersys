# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ordersys', '0010_auto_20150610_1602'),
    ]

    operations = [
        migrations.AddField(
            model_name='lineitem',
            name='printed',
            field=models.BooleanField(default=False, verbose_name=b'\xe6\x98\xaf\xe5\x90\xa6\xe5\x87\xba\xe5\x8d\x95'),
        ),
        migrations.AlterField(
            model_name='order',
            name='line_item',
            field=models.ManyToManyField(to='ordersys.LineItem', verbose_name='\u5355\u54c1', blank=True),
        ),
    ]
