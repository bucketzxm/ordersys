# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ordersys', '0008_auto_20150519_1319'),
    ]

    operations = [
        migrations.CreateModel(
            name='Coupon',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('time', models.DateTimeField(verbose_name=b'\xe4\xbc\x98\xe6\x83\xa0\xe6\x97\xb6\xe9\x97\xb4')),
                ('info', models.TextField(verbose_name=b'\xe4\xbc\x98\xe6\x83\xa0\xe4\xbf\xa1\xe6\x81\xaf')),
            ],
        ),
        migrations.CreateModel(
            name='OrderNum',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('num', models.IntegerField(verbose_name=b'\xe9\xa4\x90\xe5\x8e\x85\xe6\x8e\x92\xe9\x98\x9f\xe5\x8f\xb7\xe7\xa0\x81')),
                ('is_used', models.BooleanField(default=False)),
            ],
        ),
    ]
