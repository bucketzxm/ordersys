# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ordersys', '0006_auto_20150513_0253'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='foods',
            new_name='items',
        ),
    ]
