# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name=b'\xe7\x9b\xae\xe5\xbd\x95\xe7\xbc\x96\xe5\x8f\xb7', primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name=b'\xe7\x9b\xae\xe5\xbd\x95\xe5\x90\x8d\xe7\xa7\xb0')),
                ('image_url', models.CharField(max_length=128, verbose_name=b'\xe5\x9b\xbe\xe7\x89\x87\xe8\xb7\xaf\xe5\xbe\x84')),
                ('description', models.TextField(verbose_name=b'\xe7\x9b\xae\xe5\xbd\x95\xe6\x8f\x8f\xe8\xbf\xb0', blank=True)),
            ],
            options={
                'verbose_name': '\u76ee\u5f55',
                'verbose_name_plural': '\u76ee\u5f55',
            },
        ),
        migrations.CreateModel(
            name='Food',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name=b'\xe9\xa3\x9f\xe7\x89\xa9\xe7\xbc\x96\xe5\x8f\xb7', primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name=b'\xe9\xa3\x9f\xe7\x89\xa9\xe5\x90\x8d\xe7\xa7\xb0')),
                ('image_url', models.CharField(max_length=128, verbose_name=b'\xe5\x9b\xbe\xe7\x89\x87\xe8\xb7\xaf\xe5\xbe\x84')),
                ('price', models.FloatField(default=0, verbose_name=b'\xe9\xa3\x9f\xe7\x89\xa9\xe4\xbb\xb7\xe6\xa0\xbc')),
                ('description', models.TextField(verbose_name=b'\xe9\xa3\x9f\xe7\x89\xa9\xe6\x8f\x8f\xe8\xbf\xb0', blank=True)),
                ('special', models.BooleanField(default=False, verbose_name=b'\xe6\x98\xaf\xe5\x90\xa6\xe7\x89\xb9\xe4\xbb\xb7')),
                ('category', models.ForeignKey(verbose_name=b'\xe7\xb1\xbb\xe5\x88\xab', to='ordersys.Category')),
            ],
            options={
                'verbose_name': '\u98df\u7269',
                'verbose_name_plural': '\u98df\u7269',
            },
        ),
        migrations.CreateModel(
            name='LineItem',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('unite_price', models.FloatField(default=0, verbose_name=b'\xe9\xa3\x9f\xe7\x89\xa9\xe4\xbb\xb7\xe6\xa0\xbc')),
                ('quantity', models.IntegerField(default=0, verbose_name=b'\xe9\xa3\x9f\xe5\x93\x81\xe6\x95\xb0\xe9\x87\x8f')),
                ('food', models.ForeignKey(default=b'', verbose_name=b'\xe9\xa3\x9f\xe5\x93\x81', to='ordersys.Food')),
            ],
            options={
                'verbose_name': '\u5355\u54c1',
                'verbose_name_plural': '\u5355\u54c1',
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('time', models.DateTimeField(verbose_name=b'\xe4\xb8\x8b\xe5\x8d\x95\xe6\x97\xb6\xe9\x97\xb4')),
                ('out_trade_num', models.CharField(max_length=128, verbose_name=b'\xe8\xae\xa2\xe5\x8d\x95\xe5\x8f\xb7')),
                ('order_num', models.IntegerField(verbose_name=b'\xe9\xa4\x90\xe5\x8e\x85\xe6\x8e\x92\xe9\x98\x9f\xe5\x8f\xb7\xe7\xa0\x81')),
                ('custom_id', models.CharField(default=b'0', max_length=256, verbose_name=b'\xe9\xa1\xbe\xe5\xae\xa2\xe4\xbc\x9a\xe5\x91\x98\xe5\x8f\xb7')),
                ('state', models.CharField(default=b'WP', max_length=50, verbose_name=b'\xe8\xae\xa2\xe5\x8d\x95\xe7\x8a\xb6\xe6\x80\x81', choices=[(b'WP', '\u7b49\u5f85\u652f\u4ed8'), (b'SP', '\u652f\u4ed8\u6210\u529f'), (b'FP', '\u652f\u4ed8\u5931\u8d25')])),
                ('money', models.FloatField(default=0, verbose_name=b'\xe8\xae\xa2\xe5\x8d\x95\xe9\x87\x91\xe9\xa2\x9d')),
                ('bonus', models.BigIntegerField(default=0, verbose_name=b'\xe4\xbc\x9a\xe5\x91\x98\xe7\xa7\xaf\xe5\x88\x86')),
                ('discount', models.FloatField(default=0, verbose_name=b'\xe6\x8a\x98\xe6\x89\xa3')),
                ('description', models.TextField(verbose_name=b'\xe8\xae\xa2\xe5\x8d\x95\xe9\x99\x84\xe5\x8a\xa0\xe6\x8f\x8f\xe8\xbf\xb0', blank=True)),
                ('foods', models.ManyToManyField(to='ordersys.LineItem', verbose_name=b'\xe9\xa3\x9f\xe7\x89\xa9', blank=True)),
            ],
            options={
                'verbose_name': '\u8ba2\u5355',
                'verbose_name_plural': '\u8ba2\u5355',
            },
        ),
        migrations.CreateModel(
            name='Sauce',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name=b'\xe8\xb0\x83\xe6\x96\x99\xe7\xbc\x96\xe5\x8f\xb7', primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name=b'\xe8\xb0\x83\xe6\x96\x99\xe5\x90\x8d\xe5\xad\x97')),
                ('price', models.FloatField(default=0, verbose_name=b'\xe5\x8d\x95\xe5\x93\x81\xe4\xbb\xb7\xe6\xa0\xbc')),
                ('image_url', models.CharField(max_length=128, verbose_name=b'\xe5\x9b\xbe\xe7\x89\x87\xe8\xb7\xaf\xe5\xbe\x84')),
                ('description', models.TextField(verbose_name=b'\xe8\xb0\x83\xe6\x96\x99\xe6\x8f\x8f\xe8\xbf\xb0', blank=True)),
            ],
            options={
                'verbose_name': '\u8c03\u6599',
                'verbose_name_plural': '\u8c03\u6599',
            },
        ),
        migrations.AddField(
            model_name='lineitem',
            name='sauces',
            field=models.ForeignKey(verbose_name=b'\xe8\xb0\x83\xe6\x96\x99', to='ordersys.Sauce'),
        ),
        migrations.AddField(
            model_name='food',
            name='sauces',
            field=models.ManyToManyField(to='ordersys.Sauce', verbose_name=b'\xe8\xb0\x83\xe6\x96\x99', blank=True),
        ),
    ]
