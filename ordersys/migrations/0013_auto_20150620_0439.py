# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ordersys', '0012_auto_20150619_0948'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='state',
            field=models.CharField(default=b'WP', max_length=50, verbose_name=b'\xe8\xae\xa2\xe5\x8d\x95\xe7\x8a\xb6\xe6\x80\x81', choices=[(b'WC', '\u7b49\u5f85\u786e\u8ba4'), (b'WP', '\u7b49\u5f85\u652f\u4ed8'), (b'SP', '\u652f\u4ed8\u6210\u529f'), (b'FP', '\u652f\u4ed8\u5931\u8d25')]),
        ),
    ]
