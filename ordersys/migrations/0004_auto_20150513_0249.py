# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ordersys', '0003_auto_20150513_0244'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lineitem',
            name='sauces',
            field=models.ForeignKey(default=b'', verbose_name=b'\xe8\xb0\x83\xe6\x96\x99', to='ordersys.Sauce', null=True),
        ),
    ]
