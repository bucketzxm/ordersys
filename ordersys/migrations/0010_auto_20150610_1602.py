# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ordersys', '0009_coupon_ordernum'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lineitem',
            name='food',
            field=models.ForeignKey(default=b'', verbose_name='\u98df\u54c1', to='ordersys.Food'),
        ),
        migrations.AlterField(
            model_name='lineitem',
            name='quantity',
            field=models.IntegerField(default=0, verbose_name='\u98df\u54c1\u6570\u91cf'),
        ),
        migrations.AlterField(
            model_name='lineitem',
            name='sauces',
            field=models.ForeignKey(verbose_name='\u8c03\u6599', blank=True, to='ordersys.Sauce', null=True),
        ),
        migrations.AlterField(
            model_name='lineitem',
            name='unite_price',
            field=models.FloatField(default=0, verbose_name='\u98df\u7269\u4ef7\u683c'),
        ),
    ]
