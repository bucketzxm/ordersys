# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ordersys', '0002_auto_20150513_0244'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lineitem',
            name='sauces',
            field=models.ForeignKey(default=b'', verbose_name=b'\xe8\xb0\x83\xe6\x96\x99', blank=True, to='ordersys.Sauce'),
        ),
    ]
