# from django.contrib import admin
# from models import *
#
# # Register your models here.
# class SauceAdmin(admin.ModelAdmin):
#     list_display = ('name', 'price')
#
#
# admin.site.register(Sauce, SauceAdmin)

#!-*- coding: utf-8 -*-
from django.contrib import admin

from models import Food, Category, Order, Sauce, LineItem, OrderNum
from utils import picEncode

class SauceAdmin(admin.ModelAdmin):
    list_display = ('name', 'price')
    def save_model(self, request, obj, form, change):
        obj.save()
        if obj.image_file!="":
            obj.image_url = obj.image_file.url
            obj.save()



class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
    def save_model(self, request, obj, form, change):
        obj.save()
        if obj.image_file!="":
            obj.image_url = obj.image_file.url
            obj.save()


class OrderAdmin(admin.ModelAdmin):
    list_display = ('time', 'order_num', 'state', 'money')


class LineItemAdmin(admin.ModelAdmin):
    list_display = ('food', )

class FoodAdmin(admin.ModelAdmin):
    list_display = ('name', 'category', 'price')

    def save_model(self, request, obj, form, change):
        obj.save()
        if obj.image_file!="":
            picEncode.resize_picture( obj.image_file )
            # must after resize , path will change
            obj.image_url = obj.image_file.url
            obj.save()

admin.site.register(Sauce, SauceAdmin)
admin.site.register(Food, FoodAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(LineItem,LineItemAdmin)
admin.site.register(OrderNum)