# -*- coding: utf-8 -*-
import json
import logging
import datetime

from django.shortcuts import render_to_response, HttpResponse, redirect, RequestContext
from django.template.loader import get_template
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import *

from models import Cart, Food, Category, LineItem, Order, OrderNum
from utils.serialize import *
from utils import functool


logger = logging.getLogger('ordersys')

ERROR_CODE = {
    '1000': {"status": "1000", "desc": "执行成功"},
    '1001': {"status": "1001", "desc": "缺少参数"},
    '1002': {"status": "1002", "desc": "执行异常"}
    # '1003':{"status": "1002", "desc": "执行错误"},
}


@csrf_exempt
def get_wait_to_pay_orders(request):
    tmp = []
    for order in Order.objects.filter(state=Order.WAIT_TO_PAY):
        tmp.append(order.to_dict())
        logger.debug('Append food: ')
        for line_item in order.foods.all():
            logger.debug('%s', line_item)
    return HttpResponse(json.dumps(tmp))


@csrf_exempt
def delete_wait_to_pay(request):
    if not request.POST.has_key('out_trade_num') or not request.POST.has_key('order_num'):
        return HttpResponse(json.dumps(ERROR_CODE['1001']))
    else:
        try:
            out_trade_num = request.POST.get('out_trade_num')
            order_num = request.POST.get('order_num')
            tmp = []
            for order in Order.objects.filter(out_trade_num=out_trade_num, order_num=order_num):
                tmp.append(order.to_dict())
            return HttpResponse(json.dumps(tmp))
        except Exception, e:
            tmp = ERROR_CODE['1002']
            tmp['desc'] += (":" + str(e))
            logger.exception(str(e))
            return HttpResponse(json.dumps(tmp))


@csrf_exempt
def confirm_wait_to_pay_to_success(request):
    if not request.POST.has_key('out_trade_num') or not request.POST.has_key('order_num'):
        return HttpResponse(json.dumps(ERROR_CODE['1001']))
    else:
        try:
            out_trade_num = request.POST.get('out_trade_num')
            order_num = request.POST.get('order_num')
            for order in Order.objects.filter(out_trade_num=out_trade_num, order_num=order_num):
                order.state = order.SUCCESS_TO_PAY

                order_num = OrderNum.objects.get(num=order.order_num)
                order_num.is_used = False
                order_num.save()

                order.save()
            return HttpResponse(json.dumps(ERROR_CODE['1000']))
        except Exception, e:
            tmp = ERROR_CODE['1002']
            tmp['desc'] += (":" + str(e))
            logger.exception(str(e))
            return HttpResponse(json.dumps(tmp))


def index(request):
    title = "首页"
    special_list = Food.objects.filter(special=True)

    cart = request.session.get('cart', None)

    if cart:
        cart = pickle_load(cart)
        item_list = cart.items
    else:
        item_list = []
    # 对应首页第二排跑马灯
    amount_dict = dict([(key, {0: "notChoiced"}) for key in special_list[3:]])
    # 对应首页第一排大的跑马灯
    big_amount_dict = dict([(key, {0: "notChoiced"}) for key in special_list[0:3]])
    for item in item_list:
        if amount_dict.get(item.food):
            amount_dict[item.food] = {item.quantity: "beChoiced"}
        if big_amount_dict.get(item.food):
            big_amount_dict[item.food] = {item.quantity: "beChoiced"}
    return render_to_response('index.html', locals())


def category(request):
    title = "目录"
    cg_list = Category.objects.all()
    return render_to_response("category.html", locals())


def dishes(request):
    if request.method == "GET":
        c = {}
        cgId = request.GET['cgId']
        try:
            category = Category.objects.filter(id=cgId)[0]
        except IndexError as e:
            logger.exception(str(e))
            category = None

        food_list = []
        title = ""
        if category:
            food_list = Food.objects.all().filter(category=category)
            title = category.name

        c['cgId'] = cgId
        c['food_list'] = food_list
        c['title'] = title

        cart = request.session.get('cart', None)

        if cart:
            item_list = pickle_load(cart).items
        else:
            item_list = []

        amount_dict = dict([(key, {0: "notChoiced"}) for key in food_list])

        for item in item_list:
            if amount_dict.has_key(item.food):
                amount_dict[item.food] = {item.quantity: "beChoiced"}

        c["amount"] = amount_dict
        return render_to_response("dishes.html", c)
    redirect('/')


def view_cart(request):
    title = "购物车"
    cart_session = request.session.get('cart', None)
    # if cart is null, pickle_load will raise error

    if cart_session:
        cart = pickle_load(cart_session)
        line_items = cart.items
        print("in view cart " , cart.total_price, request.COOKIES['sessionid'])
    return render_to_response("view_cart.html", locals())


def make_order(request):
    if request.method == "GET":
        cart_session = request.session.get('cart', None)

        if not cart_session:
            # custom did not order any thing
            # TODO hint it
            return HttpResponse("", status=503)
        else:
            cart = pickle_load(cart_session)
            order_num = request.session.get('order_num', None)
            if order_num:
                order = Order.create(cart.items, cart.total_price, order_num)
            else:
                try:
                    order = Order.create(cart.items, cart.total_price)
                    request.session['order_num'] = order.order_num
                except BaseException as e:
                    logger.error("When generate order : " + str(e))
                    return HttpResponse("can not generate order", status=500)
            request.session['out_trade_num'] = order.out_trade_num
            if order:
                return HttpResponse(order.order_num, status=200)
            else:
                return HttpResponse("", status=503)


def choose_pay_method(request):
    out_trade_num = request.session['out_trade_num']
    if out_trade_num:
        return redirect("/pay/choosePayMethod/?out_trade_num=" + str(out_trade_num))
    else:
        return HttpResponse("Order not exist")


@csrf_exempt
def add_to_cart(request, num=1):
    if request.method == "POST":
        food_id = request.POST['foodId']
        food = Food.objects.get(id=food_id)
        cart_session = request.session.get('cart', None)
        # Lineitem save in cart
        # session is empty

        if not cart_session:
            cart = Cart()
            # 购物车中不存在物品的情况
        else:
            cart = pickle_load(cart_session)
            if len(cart.items) == 0:
                cart =Cart()
                request.session.clear()

        li = LineItem()
        li.food = food
        li.unite_price = food.price
        li.quantity = num
        li.save()
        cart.add_product(li, num)

        cart.total_price = round(cart.total_price, 2)
        data = pickle_dump(cart)
        request.session['cart'] = data
        return HttpResponse(cart.total_price)
    else:
        logger.error('Get Request to add_to_cart')
        return HttpResponse('request method err')


@csrf_exempt
def cut_from_cart(request, num=1):
    if request.method == "POST":
        food_id = request.POST['foodId']
        print "cut food : %s" % (str(food_id) )
        food = Food.objects.get(id=food_id)
        cart_session = request.session.get('cart', None)
        # Lineitem save in cart
        # session is empty

        cart = pickle_load(cart_session)
        for li in cart.items:
            if li.food.id == food.id:
                li.quantity -= num
                cart.total_price -= food.price
                if li.quantity == 0:
                    cart.items.remove(li)

        cart.total_price = round(cart.total_price, 2)
        data = pickle_dump(cart)
        if cart.total_price == 0:
            request.session.clear()
        else:
            request.session['cart'] = data

        return HttpResponse(cart.total_price)
    else:
        logger.error('Get Request to cut_from_cart')
        return HttpResponse("request method err")


def clear_cart(request):
    request.session.clear()
    return redirect("/myDish")


@csrf_exempt
def get_category(request):
    categories = Category.objects.all()
    data = serializers.serialize("json", categories)
    json_list = []
    for tmp_json_obj in json.loads(data):
        tmp_json_dict = tmp_json_obj['fields']
        tmp_json_dict['id'] = tmp_json_obj['pk']
        json_list.append(tmp_json_dict)
    return HttpResponse(json.dumps(json_list))


@csrf_exempt
def get_unpay_order(request):
    # 只显示今天的订单
    today = datetime.datetime.now()
    yesterday = today - datetime.timedelta(days=1)
    orders = Order.objects.filter(time__range=(yesterday, today))
    order_json_array = []
    for o in orders:
        tmp_order_json = {'id': str(o.id), 'time': o.time.strftime('%y-%m-%d %H:%M:%S'),
                          'out_trade_num': str(o.out_trade_num),
                          'order_num': str(o.order_num),
                          'custom_id': str(o.custom_id), 'state': o.state, 'money': str(o.money), 'bonus': str(o.bonus),
                          'discount': str(o.discount), 'description': o.description, 'line_item': []}
        for i in o.line_item.all():
            tmp_line_item_json = {'id': str(i.id), 'unite_price': str(i.unite_price), 'quantity': str(i.quantity),
                                  'sauces': None}

            # 处理食物
            tmp_food = i.food
            tmp_food_json = {'id': str(tmp_food.id), 'name': tmp_food.name, 'image_url': tmp_food.image_url,
                             'price': str(tmp_food.price), 'description': tmp_food.description,
                             'special': str(tmp_food.special),
                             'sauces': None}
            tmp_category = tmp_food.category
            tmp_category_json = {'id': str(tmp_category.id), 'name': tmp_category.name,
                                 'image_url': tmp_category.image_url,
                                 'description': tmp_category.description}
            tmp_food_json['category'] = tmp_category_json
            tmp_line_item_json['food'] = tmp_food_json

            # 处理配料
            tmp_sauces = i.sauces
            if tmp_sauces:
                tmp_sauces_json = {'id': str(tmp_sauces.id), 'name': tmp_sauces.name, 'price': str(tmp_sauces.price),
                                   'image_url': tmp_sauces.image_url, 'description': tmp_sauces.description}
                tmp_line_item_json['sauces'] = tmp_sauces_json
            tmp_order_json['line_item'].append(tmp_line_item_json)
        order_json_array.append(tmp_order_json)
    # print order_json_array
    return HttpResponse(json.dumps(order_json_array))


@csrf_exempt
def change_order_state(request):
    out_trade_num = request.POST.get('out_trade_num', None)
    state = request.POST.get('state', None)
    if out_trade_num and state:
        try:
            order = Order.objects.get(out_trade_num=out_trade_num)
        except ObjectDoesNotExist as e:
            logger.e(str(e))
            return HttpResponse(str(e), status=503)

        order_st = [st[0] for st in Order.ORDER_STATE]
        if state in order_st:
            order.state = state
            order.save()
            return HttpResponse("success", status=200)
    else:
        return HttpResponse("params error", status=503)


@csrf_exempt
def change_line_item_from_order(request):
    '''
    out_trade_num;
    line_item_id;
    num
    :param request:
    :return:
    '''
    out_trade_num = request.POST.get('out_trade_num', None)
    line_item_id = request.POST.get('line_item_id', None)
    num = request.POST.get('num')

    if out_trade_num and line_item_id and num:
        try:
            order = Order.objects.get(out_trade_num=out_trade_num)
            line_item = LineItem.objects.get(id=line_item_id)

        except ObjectDoesNotExist as e:
            logger.error(str(e))
            return HttpResponse("params error", status=503)

        if num < 0:
            return HttpResponse("Line Item quantity less than zero", status=503)

        if line_item in order.line_item.all():
            line_item.quantity = num
            try:
                if line_item.quantity == 0:
                    order.remove(line_item)
                    order.save()
                    order.update_total_money()
                else:
                    line_item.save()
                    order.save()
                    order.update_total_money()
            except BaseException as e:
                logger.error(str(e))
                return HttpResponse(str(e), status=503)

            return HttpResponse("change success", status=200)

        else:
            return HttpResponse("order: " + str(out_trade_num) + " has no line item " + str(line_item.id))

    else:
        logger.error("Params not complete")
        return HttpResponse("order: " + str(out_trade_num) + "params error")


def updateOrderNum(out_trade_num):
    try:
        order = Order.objects.get(out_trade_num=out_trade_num)
        total_money = order.update_total_money()
        return total_money
    except ObjectDoesNotExist as e:
        logger.error("Order not found" + str(e))
        return HttpResponse("Objects not found")


from utils import picEncode
import os


@csrf_exempt
def upload_picture(request):
    if request.method == 'POST':
        content = request.POST['content']
        file_name = request.POST['file_name']
        food_id = request.POST['food_id']

        if food_id and content and file_name:
            try:
                food = Food.objects.get(id=food_id)
                base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
                p_pos = file_name.find('.')
                if file_name[p_pos + 1:]:
                    picEncode.decode_picture(content, os.path.join(base_dir, 'media/food'), file_name)
                    food.image_url = "/media/food/" + file_name
                    try:
                        food.save()
                        return HttpResponse("success", status=200)
                    except BaseException as e:
                        logger.error(e)
                        return HttpResponse("save object failed", status=503)
                else:
                    return HttpResponse("Not a image file", status=503)
            except ObjectDoesNotExist as e:
                logger.error("Function: upload_picture food not exist")
                return HttpResponse("food_id error", status=503)

        else:
            return HttpResponse("params error", status=503)


def confirm_order(request):
    title = "确认订单"
    cart = request.session.get('cart', None)
    # if cart is null, pickle_load will raise error
    if cart:
        cart = pickle_load(cart)
    line_items = []
    if not cart:
        # 购物车空
        line_items = []
    else:
        line_items = cart.items

    t = get_template("confirm_order.html")
    c = RequestContext(request, locals())
    return HttpResponse(t.render(c))


def query_order_by_orderNum(request):
    if request.method == "GET":
        ret_dict = functool.get_proper_params(request.GET, ['order_num'])
        if ret_dict['error']:
            return HttpResponse("Lack of params %s" % ret_dict['error'], status=503)
        else:
            order_li = Order.objects.filter(order_num=ret_dict['order_num'], state='WC').order_by("time").reverse()
            try:
                order = order_li[0]
            except IndexError as e:
                if request.session.get('cart'):
                     request.session.clear()
                return redirect('/')

            cart = Cart.init_cart(order.line_item.all())
            request.session.clear()
            request.session['cart'] = pickle_dump(cart)
            request.session['order_num'] = order.order_num
            request.session['out_trade_num'] = order.out_trade_num
            print("deleted", cart.total_price)
            return redirect("/")
    else:
        return HttpResponse("Method error", status=503)