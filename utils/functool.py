__author__ = 'simon'


def check_params(dict_we_have,list_we_want,*args,**kwargs):
    '''
    check if we have all key-value we want
    :param dict_we_have:
    :param list_we_want:
    :param args:
    :param kwargs:
    :return: a list of lacked key-value
    '''
    lacked = {"lacked_params":[], "error":""}
    for key in list_we_want:
        if dict_we_have.get(key) == None:
            lacked["lacked_params"].append(key)

    if lacked['lacked_params']:
        lacked['error'] = "Lack of params %s" % (str(lacked['lacked_params']))
    return lacked



def get_proper_params(dict_we_have,list_we_want, *args, **kwargs):
    try:
        ret_dict = check_params(dict_we_have, list_we_want)
        if ret_dict['error']:
            return ret_dict
        else:
            for key in list_we_want:
                ret_dict[key] = dict_we_have[key]

        return ret_dict
    except:
        ret_dict = {"errors": "can not get proper params you want"}
        return ret_dict