__author__ = 'simon'

import base64, zlib, os, glob
from PIL import Image


def decode_picture(content, save_path, filename):
    with open(os.path.join(save_path, filename), "w") as f:
        f.write(base64.decodestring(content))


def upload_picture(file_path):
    with open(file_path, "rb") as f:
        raw_content = f.read()
        return base64.encodestring(raw_content)

from WeiyueOrderSys import settings
def resize_picture(pic):
    # w, h = pic.get_image_dimensions()
    path = os.path.join( settings.MEDIA_ROOT,pic.name)
    im = Image.open( path )
    im_ss = im.resize((int(400 * 0.7), int(400)), Image.BILINEAR)
    im_ss.save( path )

if __name__ == "__main__":
    import sys, os

    content = upload_picture(sys.argv[1])
    print content
    save_path = os.path.dirname(os.path.abspath(__file__))

    decode_picture(content, save_path, "test.jpg")